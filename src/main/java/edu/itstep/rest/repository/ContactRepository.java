package edu.itstep.rest.repository;


import edu.itstep.rest.entity.Contact;

import java.util.List;

public interface ContactRepository {
    List<Contact> getAll();

    void saveOrUpdate(Contact contact);

    void deleteById(int id);

    Contact getById(int id);
}
