package edu.itstep.rest.contoller;

import edu.itstep.rest.entity.Contact;
import edu.itstep.rest.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class ContactController {
    @Autowired
    private ContactRepository contactRepository;


    //@RequestMapping(method = RequestMethod.GET, path = "/employees")
    @GetMapping("/contacts")
    public List<Contact> getAll() {
        return contactRepository.getAll();
    }

    @GetMapping("/contacts/{id}")
    public Contact getById(@PathVariable int id) {
        return contactRepository.getById(id);
    }

    @PostMapping("/contacts")
    public Contact save(@RequestBody Contact contact){
        contactRepository.saveOrUpdate(contact);
        return contact;
    }

    @PatchMapping("/contacts")
    public Contact update(@RequestBody Contact contact){
        System.out.println(contact);
        contactRepository.saveOrUpdate(contact);
        return contact;
    }

    @DeleteMapping("/contacts/{id}")
    public void deleteById(@PathVariable int id){
        contactRepository.deleteById(id);
    }

}
